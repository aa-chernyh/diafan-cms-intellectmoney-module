<?php

/**
 * Настройки системы IntellectMoney для административного интерфейса
 */
if (!defined('DIAFAN')) {
    $path = __FILE__;
    $i = 0;
    while (!file_exists($path . '/includes/404.php')) {
        if ($i == 10)
            exit;
        $i++;
        $path = dirname($path);
    }
    include $path . '/includes/404.php';
}

class Payment_intellectmoney_admin {

    public $config;

    public function __construct() {
        $this->config = array(
            "name" => 'IntellectMoney',
            "params" => array(
                'intellectmoney_inn' => array(
                    'name' => 'ИНН организации',
                    'help' => 'ИНН организации, необходим для формирования чека'
                ),
                'intellectmoney_eshopId' => array(
                    'name' => 'Номер магазина в системе IntellectMoney',
                    'help' => 'Номер магазина нужно скопировать из личного кабинета IntellectMoney'
                ),
                'intellectmoney_secretKey' => array(
                    'name' => 'Секретный ключ в системе IntellectMoney',
                    'help' => 'Укажите секретный ключ, такой же, который вы указали в личном кабинете IntellectMoney'
                ),
                'intellectmoney_test' => array(
                    'name' => 'Тестовый режим',
                    'type' => 'checkbox',
                    'help' => 'Включите данный режим при тестировании'
                ),
                'intellectmoney_hold' => array(
                    'name' => 'Режим холдирования',
                    'type' => 'checkbox',
                    'help' => 'Включите данный режим, если хотите холдировать денежные средства'
                ),
                'intellectmoney_expireDate' => array(
                    'name' => 'Дней в холде',
                    'type' => 'text',
                    'help' => 'Данный параметр относится к режиму холдирования. Укажите количество дней, по истечению которых денежные средства автоматически зачислятся на ваше счет, либо будут возвращены клиенту. Максимум 30 дней. По умолчанию 3 дня.'
                ),
                'intellectmoney_preference' => array(
                    'name' => 'Доступные способы оплаты',
                    'help' => "По умолчанию доступны все способы оплаты. Оставьте данное поле пустым, если не знаете что указать"
                ),
                'intellectmoney_nds' => array(
                    'name' => 'Ставка НДС',
                    'type' => 'select',
                    'select' => array(
                        '1' => 'НДС 20%',
                        '2' => 'НДС 10%',
                        '3' => 'НДС расч. 20/120',
                        '4' => 'НДС расч. 10/110',
                        '5' => 'НДС 0%',
                        '6' => 'НДС не облагается'
                    ),
                )
            )
        );
    }

}
