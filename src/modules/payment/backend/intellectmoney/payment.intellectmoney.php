<?php

if (!defined('DIAFAN')) {
    $path = __FILE__;
    $i = 0;
    while (!file_exists($path . '/includes/404.php')) {
        if ($i == 10)
            exit;
        $i++;
        $path = dirname($path);
    }
    include $path . '/includes/404.php';
}

if (empty($_REQUEST["orderId"])) {
    echo "Don't set orderId";
    exit();
} else {
    $pay = $this->diafan->_payment->check_pay($_REQUEST["orderId"], 'intellectmoney');
}

if ($_GET["rewrite"] == "intellectmoney/result") {

    ob_start();

    $order_secretKey = $pay['params']['intellectmoney_secretKey'];
    $valid = false;

    if (isset($_REQUEST['secretKey']) && !empty($_REQUEST['secretKey'])) {
        $valid = check_secretKey($order_secretKey);
    } else {
        $valid = check_hash($order_secretKey);
    }

    if ($valid === true) {
        $paymentStatus = $_REQUEST['paymentStatus'];
        if ($paymentStatus == 3) {
            ob_exit();
        } elseif ($paymentStatus == 5) {
            $this->diafan->_payment->success($pay, 'pay');
        }
        //Холд
        elseif ($paymentStatus == 6) {
            ob_exit();
        }
        //частично оплачен
        elseif ($paymentStatus == 7) {
            ob_exit();
        }
        //Отменен
        elseif ($paymentStatus == 4) {
            ob_exit();
        }
    } else {
        ob_exit($valid);
    }
    ob_exit();
    exit;
}

if ($_GET["rewrite"] == "intellectmoney/success") {
    $this->diafan->_payment->success($pay, 'redirect');
}

function ob_exit($status = null) {
    if ($status) {
        ob_end_flush();
        isset($_REQUEST['debug']) ? exit($status) : exit();
    } else {
        ob_end_clean();
        header("HTTP/1.0 200 OK");
        echo "OK";
        exit();
    }
}

function check_secretKey($key) {
    if ($_REQUEST['secretKey'] == $key)
        return true;
    else
        return "SecretKey don't match";
}

function check_hash($key) {

    $is_encoded = preg_match('~%[0-9A-F]{2}~i', $_REQUEST['paymentData']);
    if ($is_encoded) {
        list($eshopId, $orderId, $serviceName, $eshopAccount, $recipientAmount, $recipientCurrency, $paymentStatus, $userName, $userEmail, $paymentData, $hash) = array($_REQUEST['eshopId'], urldecode($_REQUEST['orderId']), urldecode($_REQUEST['serviceName']), $_REQUEST['eshopAccount'], $_REQUEST['recipientAmount'], $_REQUEST['recipientCurrency'], $_REQUEST['paymentStatus'], urldecode($_REQUEST['userName']), urldecode($_REQUEST['userEmail']), urldecode($_REQUEST['paymentData']), $_REQUEST['hash']);
    } else {
        list($eshopId, $orderId, $serviceName, $eshopAccount, $recipientAmount, $recipientCurrency, $paymentStatus, $userName, $userEmail, $paymentData, $hash) = array($_REQUEST['eshopId'], $_REQUEST['orderId'], $_REQUEST['serviceName'], $_REQUEST['eshopAccount'], $_REQUEST['recipientAmount'], $_REQUEST['recipientCurrency'], $_REQUEST['paymentStatus'], $_REQUEST['userName'], $_REQUEST['userEmail'], $_REQUEST['paymentData'], $_REQUEST['hash']);
    }

    $control_hash_str = implode('::', array(
        $eshopId, $orderId, $serviceName,
        $eshopAccount, $recipientAmount, $recipientCurrency, $paymentStatus,
        $userName, $userEmail, $paymentData, $key
    ));

    $control_hash = md5($control_hash_str);
    $control_hash_win1251 = md5(mb_convert_encoding($control_hash_str, 'windows-1251'));
    $control_hash_utf8 = md5(mb_convert_encoding($control_hash_str, 'utf-8'));

    if ($control_hash == $hash || $control_hash_utf8 == $hash || $control_hash_win1251 == $hash)
        return true;
    else {
        echo "Hash don't match\n\n<br/><br/>";
        return "control_hash_str: $control_hash_str"
                . "\n<br/>"
                . "control_hash_str_win1251: " . mb_convert_encoding($control_hash_str, 'windows-1251')
                . "\n<br/>"
                . "control_hash_str_utf8: " . mb_convert_encoding($control_hash_str, 'utf-8')
                . "\n<br/>"
                . "\n<br/>"
                . "control_hash: $control_hash"
                . "\n<br/>"
                . "control_hash_win1251: $control_hash_win1251"
                . "\n<br/>"
                . "control_hash_utf8: $control_hash_utf8"
                . "\n<br/>"
                . "\n<br/>"
                . "hash: $hash";
    }
}
