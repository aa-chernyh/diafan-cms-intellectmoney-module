<?php

if (!defined('DIAFAN')) {
    $path = __FILE__;
    $i = 0;
    while (!file_exists($path . '/includes/404.php')) {
        if ($i == 10)
            exit;
        $i++;
        $path = dirname($path);
    }
    include $path . '/includes/404.php';
}

class Payment_intellectmoney_model extends Diafan {

    public function get($params, $pay) {
        if (!empty($pay['details']['discount'])) {
            foreach ($pay["details"]["goods"] as $row) {
                $orderSumm = ($row["price"] * $row["count"]) + $orderSumm;
            }
            $taxDiscount = ($orderSumm - $pay['details']['discount']) / $orderSumm;
        } else {
            $taxDiscount = 1;
        }

        $merchantReceipt = array(
            'inn' => $params['intellectmoney_inn'],
            'group' => 'Main',
            'content' => array(
                'type' => 1,
                'positions' => array(),
                'customerContact' => $pay['details']['email']
            ),
        );

        $totalPosAmount = 0;
        foreach ($pay['details']['goods'] as $details) {
            $posAmount = number_format($details['price'] * $taxDiscount, 2, '.', '');
            $quantity = number_format($details['count'], 3, '.', '');
            $merchantReceipt['content']['positions'][] = array(
                'quantity' => $quantity,
                'price' => $posAmount,
                'tax' => $params['intellectmoney_nds'],
                'text' => substr($details['name'], 0, 128),
            );
            $totalPosAmount += $posAmount * $quantity;
        }

        if ($pay['details']['delivery']['summ'] != 0) {
            $merchantReceipt['content']['positions'][] = array(
                'quantity' => 1,
                'price' => number_format($pay['details']['delivery']['summ'], 2, '.', ''),
                'tax' => $params['intellectmoney_nds'],
                'text' => substr('Доставка', 0, 128),
            );
            $totalPosAmount = number_format($pay['details']['delivery']['summ'] + $totalPosAmount, 2, '.', '');
        }

        $totalOrderAmount = number_format(floatval($pay['summ']), 2, '.', '');

        $difference = number_format($totalOrderAmount - $totalPosAmount, 2, '.', ''); //не менять местами;

        if ($difference != '0.00') {
            $merchantReceipt['content']['positions'][0]['price'] = number_format($merchantReceipt['content']['positions'][0]['price'] + $difference, 2, '.', '');
        }

        $merchantReceipt = json_encode($merchantReceipt);
        $postData = array(
            'eshopId' => $params['intellectmoney_eshopId'],
            'orderId' => $pay['id'],
            'serviceName' => $pay['desc'],
            'recipientAmount' => $totalOrderAmount,
            'recipientCurrency' => $params['intellectmoney_test'] ? 'TST' : 'RUB',
            'email' => $pay['details']['email'],
            'userName' => $pay['details']['name'],
            'preference' => $params['intellectmoney_preference'],
            'successUrl' => 'http://' . $_SERVER['HTTP_HOST'] . '/payment/get/intellectmoney/success/?orderId=' . $pay['id'],
            'merchantReceipt' => $merchantReceipt,
            'UserField_1' => 'phone_number:' . $pay['details']['phone'],
            'UserField_2' => 'comment:' . $pay['details']['comment']
        );
        if ($params['intellectmoney_hold']) {
            $postData['holdMode'] = '1';
            $day = $params['intellectmoney_expireDate'];
            if (intval($day) && $day > 0 && $day < 31) {
                $postData['expireDate'] = date('Y-m-d H:i:s', strtotime('+' . $day . ' day'));
            } else {
                $postData['expireDate'] = date('Y-m-d H:i:s', strtotime('+3 day'));
            }
        }
        $url = 'https://merchant.intellectmoney.ru/ru/?';
        foreach ($postData as $name => $value) {
            $url .= $name . '=' . urlencode($value) . '&';
        }
        $link = trim($url, '&');
        $this->diafan->redirect($link);
        exit;
    }

}
