#Модуль оплаты платежной системы IntellectMoney для CMS Diafan

> **Внимание!** <br>
Данная версия актуальна на *25 марта 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/display/TECH/Diafan.cms#diafan_files.

Страница модуля на marketplace: https://addons.diafan.ru/modules/platezhnye-moduli/platezhnyy-modul-intellectmoney/
<br>
Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/display/TECH/Diafan.cms#557475d3f8d7eebaaf47679d13f41b984226ce
<br>
Ответы на частые вопросы можно найти здесь: https://wiki.intellectmoney.ru/display/TECH/Diafan.cms#557475ab8e0de3144e4139b936195663dd5b34
